import java.util.Scanner;
public class WDH {

	//Dekaration einer Methode
	
	public static int quadrieren (int m_ganzeZahl) {
		int ergebnis;
		ergebnis = m_ganzeZahl * m_ganzeZahl;
		return ergebnis;
	}	//end of quadrieren
	
	public static int multipliziren (int m_zahl, int m_zahl2) {
		int ergebnis;
		ergebnis = m_zahl * m_zahl2;
		return ergebnis;
	}	
	
	public static void main(String[] args) {
		// Deklaration eines Scanner
		Scanner myScanner = new Scanner (System.in);
		
		//Deklaration weiterer Variablen 
		//double doubleVariablen;
		//String meinText;
		//char zeichenerg;
		int ganzeZahl, erg, ganzeZahl2;
		
		System.out.println("Geben Sie eine ganze Zahl ein:");
		ganzeZahl = myScanner.nextInt();
		
		System.out.println("Geben Sie eine zweite ganze Zahl ein:");
		ganzeZahl2 =myScanner.nextInt();
		
		erg = quadrieren(ganzeZahl);
		System.out.println(ganzeZahl + " hoch2 = " + erg);
		
		erg = multipliziren(ganzeZahl, ganzeZahl2);
		System.out.println(ganzeZahl + "*" + ganzeZahl2 + " = " + erg);

		
	}

}
