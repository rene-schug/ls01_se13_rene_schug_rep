
public class Konsolenausgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		//zwei S�tzte mit zwei Ausgabebefehle ohne Zeilenumbruch.
		System.out.print("Du B�ser Mensch. ");
		System.out.println("Ein B�ser Mensch bist du.");
		
		System.out.println("");
		
		System.out.println("Du \"B�ser\" Mensch.");
		System.out.println("Ein B�ser Mensch bist du");
		
		/* Println erzugt ein Zeilenumbruch
		 * und Print erzeugt keinen.
		 */
		
	}

}
