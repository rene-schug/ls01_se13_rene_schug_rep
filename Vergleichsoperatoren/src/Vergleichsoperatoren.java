/**
 *
 * Beschreibung
 *
 * @version 1.0 vom 27.10.2020
 * @author 
 */

public class Vergleichsoperatoren {
  
  public static void main(String[] args) {
    
    int c = 10, d = 5; 
    char buchstabe = 'f'; 
    boolean e = true;
    
    System.out.println(c < 9 || d >= 11);
    System.out.println(c > 9 || (d > 7 && buchstabe == 'f'));
    System.out.println((c < 5 || d < 11) && (buchstabe != 'a' || c == 4));
    System.out.println(!(c == 10) && d <= 11);
    System.out.println(!(c == 10));
    System.out.println(e);
    System.out.println(!(c != 10) && ((!(buchstabe != 'f') && c < 11) && !e));
        
    
    int a = 2, b = 3;
    char name = 'Q';
    
    System.out.print("System.out.println(a < b);\t\t");
    System.out.println(a < b);
    System.out.print("System.out.println(2*a <= b);\t\t");
    System.out.println(2*a <= b);
    System.out.print("System.out.println(name != 'Q');\t");
    System.out.println(name != 'Q');
    System.out.print("System.out.println(2*a == a + b -1);\t");
    System.out.println(2*a == a + b -1);
    System.out.print("System.out.println(name == 81);\t\t");
    System.out.println(name == 81);
    System.out.print("System.out.println(b-a > 1);\t\t");
    System.out.println(b-a > 1);
    System.out.print("System.out.println(a != b-1);\t\t");
    System.out.println(a != b-1);
  
  
    int x = 2, y = 5, z = 1;
    System.out.println (x++);
    System.out.println (++x);
    x = 1;
    x += y;
    System.out.println (x);
    System.out.println (-y - +y); 
    System.out.println (y/x);
    x = 2;
    System.out.println (y%x);
    //System.out.println (y%--z);
    System.out.println(y + " " + x);    //zur Orientierung
    System.out.println (y+++x++);
    System.out.println(y + " " + x);   //zur Orientierung
    System.out.println (++y + ++x);    //als Beispiel
    
  } // end of main

} // end of class Vergleichsoperatoren

