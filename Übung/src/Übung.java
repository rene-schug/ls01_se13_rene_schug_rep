import java.util.Scanner;

class Übung
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
     
       double zuZahlenderBetrag;
       double eingezahlterGesamtbetrag;
       double eingeworfeneMuenze;
       double rueckgabebetrag;
       

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();

       
       
       	   
    		   
    		   
    		   
       // Geldeinwurf
       // -----------
       zuZahlenderBetrag = zuZahlenderBetrag;
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
      System.out.format("%s %.2f %s\n","Noch zu zahlen: ", zuZahlenderBetrag - eingezahlterGesamtbetrag, "Euro");
      System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
      eingeworfeneMuenze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMuenze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
Thread.sleep(250);
} catch (InterruptedException e) {
// TODO Auto-generated catch block
e.printStackTrace();
}
       }
       System.out.println("\n\n");

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rueckgabebetrag > 0.0)
       {
      System.out.println("Der R�ckgabebetrag in H�he von " + rueckgabebetrag + " EURO");
      System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
         System.out.println("2 EURO");
         rueckgabebetrag -= 2.0;
           }
           while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
         System.out.println("1 EURO");
         rueckgabebetrag -= 1.0;
           }
           while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
         System.out.println("50 CENT");
         rueckgabebetrag -= 0.5;
           }
           while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
         System.out.println("20 CENT");
           rueckgabebetrag -= 0.2;
           }
           while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
         System.out.println("10 CENT");
         rueckgabebetrag -= 0.1;
           }
           while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
         System.out.println("5 CENT");
           rueckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}