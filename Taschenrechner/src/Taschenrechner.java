
import java.util.Scanner;
public class Taschenrechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Deklaration eines Scanner
		Scanner myScanner = new Scanner (System.in);
		
		double zahl1 = 0, zahl2 = 0, ergebnis = 0;
		
		System.out.print("Bitte geben Sie eine Zahl ein    : ");
		zahl1 = myScanner.nextDouble () ;
		
		System.out.print("Bitte geben Sie eine zweite Zahl ein    : ");
		zahl2 = myScanner.nextDouble () ;
		
		ergebnis = zahl1 + zahl2;
		
		System.out.printf("Ergebnis der Addition lautet:  %.2f ",  ergebnis );
		
	}
}
